class Solution:
    # @param a : list of integers
    # @param b : integer
    # @return a list of integers
    def rotateArray(self, a, b):
        ret, arrayLength = [], len(a)
        if b > 0:
            b %= arrayLength
        ret.extend(a[b:]+ a[0:b])
        return ret
