class Solution:
    # @param A : integer
    # @return an integer
    def reverse(self, A):
        if(A > 0):
            res = int("".join(reversed(str(A))))
            return res * (res < 2 ** 31)
        res = -1 * int("".join(reversed(str(A)[1:])))
        return res * (res >-2 ** 31)
