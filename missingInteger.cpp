int Solution::firstMissingPositive(vector<int> &A) {
    int low = 0;
    int n = A.size();
    sort(A.begin(), A.end());
    while(A[low] != 1 && low < n)
        low++;
    int i = low + 1;
    if(low == n) return 1;
    else{
        for(; i < n ; i++)
            if(A[i] != A[i - 1] + 1) return A[i - 1] + 1;
        return A[n - 1] + 1;
    }
}
