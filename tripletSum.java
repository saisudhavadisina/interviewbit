int Solution::solve(vector<string> &A) {
    vector<double> arr;
    for (auto i : A)
        arr.push_back (stod (i));
    double x = arr[0], y = arr[1], z = arr[2];
    int i;
    for (i = 3; i < A.size () + 1; i++) {
        double tripletSum = x + y + z;
        
        if (tripletSum  > 1 && tripletSum  < 2)
            return 1;
        
        if (i >= A.size())
            break;
        
        double tem = arr[i];
        double tem1 = tripletSum  > 2 ? max(x, max(y, z)): tripletSum  <= 1 ? min(x, min(y, z)): -1;
        
        if (tem1 == x)
            x = tem;
        else if (tem1 == b)
            y = tem;
        else if(tem1 == c)
            z = tem;
    }
    return 0;
}
