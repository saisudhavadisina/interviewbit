class Solution:
    # @param A : list of integers
    # @return a list of integers
    def subUnsort(self, A):
        temp, n = [i for i in A], len(A)
        A.sort()
        low, high, fixed_high = -1, -1, 0
        for i in range(n):
            if fixed_high and A[i] != temp[i]:
                fixed_high = 0
            if low == -1:
                if A[i] != temp[i]:
                    low = i
            if not fixed_high:
                if A[i] == temp[i]:
                    high, fixed_high = i - 1, 1
        if high == -1 or fixed_high == 0:
            high = n - 1
        if low == -1:
            return [-1]
        return [low, high]
