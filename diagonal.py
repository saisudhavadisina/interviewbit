class Solution:
    # @param A : list of list of integers
    # @return a list of list of integers
    def diagonal(self, A):
        res, temp =[], set()
        for i in range(len(A)):
            for j in range(len(A[0])):
                if (i, j) not in temp:
                    tp1, a, b = [], i, j
                    while a < len(A) and b >= 0:
                        tp1.append(A[a][b])
                        temp.add((a,b))
                        a, b = a + 1, b - 1
                    res.append(tp1)
        return res
