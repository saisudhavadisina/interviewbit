class Solution:
    # @param A : string
    # @return an integer
    def titleToNumber(self, A):
        res, size = [], len(A)
        for i in range(size - 1, -1, -1):
            res.append((26 ** i) * (ord(A[size - (i + 1)]) - 64))
        return sum(res)
