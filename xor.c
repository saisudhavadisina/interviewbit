int diff(const void *, const void *);

int findMinXor(int* A, int n1) {
    qsort(A, n1, sizeof(int), diff);
    int i = n1, res = A[0] ^ A[1];
    while(i > 1){
        if((A[i - 1] ^ A[i]) < res)
            res = (A[i - 1] ^ A[i]);
        i--;
    }
    return res;
} 

int diff(const void * m, const void * n)
{
   return (*(int*)m - *(int*)n);
}
