vector<int> Solution::flip(string A) {
    vector<int> bitValue(A.size()), res;
    for(int i = 0; i < A.size(); i++){
        if(A[i] == '0') //checking whether the bit is zero or not
            bitValue[i] = 1;
        
        else
            bitValue[i] = -1;
    }
    int cumsum = 0, start = 0, end = 0, total = INT_MIN, tot1 = 0, tot2 = 0;
    for(int i = 0; i < A.size(); i++){
        cumsum += bitValue[i]; //calculating cummulative sum and storing in a variable
        
        if(cumsum < 0){
            start = i + 1;
            cumsum = 0;
        }
        else if(cumsum > total){
            tot1 = start;
            tot2 = i;
            total = cumsum;
        }
    }
    if(total <= 0)
        return res;
    else{
        res.push_back(tot1 + 1);
        res.push_back(tot2 + 1);
    }
    
    return res;
}
