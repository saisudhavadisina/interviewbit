class Solution:
    # @param A : integer
    # @return a list of list of integers
    def generateMatrix(self, A):
        row, col, tem = A - 1, 0, 1
        spiral_matrix = [[0 for i in range(A)] for j in range(A)]
        matrix = int((A + 1) / 2)
        for _ in range(matrix):
            for i in range(col, row + 1):
                spiral_matrix[_][i] = tem
                tem += 1
            for i in range(col + 1, row + 1):
                spiral_matrix[i][row] = tem
                tem += 1
            for i in range(row - 1, col - 1, -1):
                spiral_matrix[row][i] = tem
                tem += 1
            for i in range(row - 1, col, -1):
                spiral_matrix[i][col] = tem
                tem += 1
            col += 1
            row -= 1
        return spiral_matrix
