from math import factorial as fact
class Solution:
    # @param A : integer
    # @return a list of integers
    def getRow(self, A):
        res = []
        for i in range(A + 1):
            res.append(int(fact(A)/(fact(i) * fact(A-i))))
        return res
