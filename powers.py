class Solution:
    # @param A : integer
    # @return an integer
    def isPower(self, A):
        if A == 1: 
            return 1
        for i in range(2, (int)(math.sqrt(A)) + 1) : 
            j = 2
            P = (int)(math.pow(i, j))
            while (P <= A and P > 0) : 
                if (P == A) : 
                    return 1
                j += 1
                P = math.pow(i, j) 
        return 0
