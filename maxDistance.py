class Solution:
    # @param A : tuple of integers
    # @return an integer
    def maximumGap(self, A):
        arr = list(A)
        index = {}
        for i in range(len(arr)):
            if arr[i] in index:
                index[arr[i]].append(i)
            else:
                index[arr[i]] = [i]
        arr.sort(reverse  = False)
        diff = 0
        temp = len(arr)
        for i in range(len(arr)):
            if temp > index[arr[i]][0]:
                temp = index[arr[i]][0]
            diff = max(diff, index[arr[i]][-1] - temp)
        return diff 
