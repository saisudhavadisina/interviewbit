class Solution:
    # @param A : tuple of integers
    # @return an integer
    def maximumGap(self, A):
        A, res, size = list(A), [], len(A)
        A.sort()
        res = [A[i + 1] - A[i] for i in range(size - 1) if(A[i + 1] - A[i] > res)]
        return max(res)
