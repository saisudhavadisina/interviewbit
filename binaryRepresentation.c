#include <stdio.h>

void binaryRepresentation(int num) { 
    if (num > 1) 
        binaryRepresentation(num / 2); 
    printf("%d", num % 2); 
}

int main(){
	int n;
	scanf("%d", &n);
	binaryRepresentation(n);
	return 0;
}
