class Solution:
    # @param A : integer
    # @param B : integer
    # @return an integer
    def gcd(self, A, B):
        return A if B == 0 else self.gcd(B, A % B)

if __name__ == "__main__":
    A, B = map(int, input().split())
    obj = Solution()
    print(obj.gcd(A, B))
